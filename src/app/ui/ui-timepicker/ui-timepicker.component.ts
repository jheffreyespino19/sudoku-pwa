import {Component, ElementRef,  OnInit, ViewChild} from '@angular/core';
import {UiBaseComponent} from '../ui-base.component';
import {isNullOrUndefined} from 'util';
import {LanguageService} from "../../core/language.service";
import { FormControl } from '@angular/forms';

@Component({
  selector: 'qnect-ui-timepicker',
  templateUrl: './ui-timepicker.component.html',
  styleUrls: ['../ui-base.component.scss', './ui-timepicker.component.scss']
})
export class UiTimepickerComponent extends UiBaseComponent implements OnInit {
  timeSelect: Date;
  minuteStep = 1;

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.formControl.value) {
      const val = this.formControl.value;
      if (val instanceof Date) {
        this.setTimeValue(val);
      }
    }
  }

  getTimeValue() {
    return this.timeSelect;
  }

  setTimeValue(date: Date) {
    if (isNullOrUndefined(date)) {
      // Default to 12 Hours 00 Minutes
      const newDate = new Date();
      newDate.setHours(12);
      newDate.setMinutes(0);
      this.timeSelect = newDate;
    } else {
      this.timeSelect = date;
    }
  }
}
