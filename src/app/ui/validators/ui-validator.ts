import {AbstractControl} from '@angular/forms';
import {DateService} from '../ui-datepicker/date.service';

function isEmptyInputValue(value: any): boolean {
  // we don't check for string here so it also works with arrays
  return value == null || value.length === 0;
}

export class UiValidators {

  static get ERROR_CODES(): any {
    return {
      required: {
        'message': 'Required',
        'code': 'required'
      },
      pattern: {
        'message': 'Invalid format',
        'code': 'pattern'
      },
      rangeMin: {
        'message': 'Value is lower than the minimum allowed',
        'code': 'rangeMin'
      },
      rangeMax: {
        'message': 'Value exceeds the maximum allowed',
        'code': 'rangeMax'
      },
      maxlength: {
        'message': 'Invalid length',
        'code': 'maxlength'
      },
      atLeastOneRequired: {
        'message': 'Either of these fields should not be empty',
        'code': 'atLeastOneRequired'
      }
    };
  };


  static getValidatorErrors(errors: any): any[] {
    let errorList: any[] = null;
    if (errors) {
      errorList = [];
      Object.keys(errors).forEach(function (key) {
        if (UiValidators.ERROR_CODES[key]) {
          errorList.push(UiValidators.ERROR_CODES[key]);
        } else {
          errorList.push({
            'message': '',
            'code': key
          });
        }
      });
    }
    return errorList;
  }


  static rangeMin = (_min: number) => {
    return (control: AbstractControl) => {
      var num = control.value;
      if (!isNaN(num) && num < _min) {
        return {'rangeMin': true};
      }
      return null;
    };
  }

  static rangeMax = (_max: number) => {
    return (control: AbstractControl) => {
      var num = control.value;
      if (!isNaN(num) && num > _max) {
        return {'rangeMax': true};
      }
      return null;
    };
  }

  static atLeastOneRequired = (fieldIdList: string[]) => {
    return (control: AbstractControl) => {
      let isAnyTouched = false;
      let isAllEmpty = true;
      for (const fieldId of fieldIdList) {
        let field = control.parent.get(fieldId);
        if (field.touched) {
          isAnyTouched = true;
        }
        if (!isEmptyInputValue(field.value)) {
          isAllEmpty = false;
        }
      }
      if (isAnyTouched && isAllEmpty) {
        return {'atLeastOneRequired': true};
      }
      return null;
    };
  }

  /**
   * Usage:
   * "validationRegexList": [{"1": "^[0-9]{18,}"},{"2": "^[0-9]{8,}"}],
   * "dictionary" : {"1" : {"en" : "Error1"},"2" : {"en" : "Erro2"}}
   * @param regexList
   * @returns {(control:AbstractControl)=>{}}
   */
  static validationRegexList = (regexList: any[]) => {
    return (control: AbstractControl) => {
      const result = {};
      for (const regex of regexList) {
        Object.keys(regex).forEach(key => {
          if (!new RegExp(regex[key]).test(control.value)) {
            result[key] = true;
          }
        });
      }
      return result;
    };
  }

  /**
   * @param
   * @returns {(control:AbstractControl)=>{}}
   */
  static timepickerPatternValidation = () => {
    return (control: AbstractControl) => {
      const value = control.value;
      if (!value) {
        return {'timePattern': true}
      }
      return null;
    };
  }

  static moneyCurrencyValidation = (formControl: AbstractControl) => {
    return (control: AbstractControl) => {
      const result = {};
      if (((control.value) && (!isNaN(formControl.value) && !formControl.value)) ||
        ((!isNaN(control.value) && !control.value) && (!isNaN(formControl.value) && formControl.value))) {
        result['moneyFieldRequired'] = true;
      }
      return result;
    };
  }

  /**
   * @param
   * @returns {(control:AbstractControl)=>{}}
   */
  static dateValidation = (dateFormat: string) => {
    return (control: AbstractControl) => {
      const date: any = DateService.convertStringToDate(control.value, dateFormat);
      if (control.value && !isNaN(Date.parse(date))) {
        // this.setDateValue(date);
        // if (date < this._minDate) {
        //   this.error('min');
        // } else {
        //   this.removeError('min');
        // }
        //
        // if (date > this.maxDate) {
        //   this.error('max');
        // } else {
        //   this.removeError('max');
        // }
      } else {
        return {'pattern': true}
      }
      return null;
    };
  }
}
