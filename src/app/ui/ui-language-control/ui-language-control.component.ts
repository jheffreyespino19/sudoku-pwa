import {Component, OnDestroy, OnInit} from '@angular/core';

import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'qnect-ui-language-control',
  templateUrl: './ui-language-control.component.html'
})
export class UiLanguageControlComponent extends UiBaseComponent implements OnInit {

  protected currentLang : string;

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {

    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.currentLang = language;
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiLanguageControlComponent:" + this.fieldId + " " + errorMessage);
        }
      );
  }

  isCurrentLang(lang: string) {
    return lang === this.currentLang;
  }

  selectLang(lang: string) {
    // set default;
    this.languageService.changeLanguage(lang);
  }

}
