import {Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';


import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'qnect-ui-radio',
  templateUrl: './ui-radio.component.html',
  styleUrls: ['./ui-radio.component.scss', '../ui-base.component.scss']
})
export class UiRadioComponent extends UiBaseComponent implements OnInit {

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.languageService.translateOptions(this.uiModel.options, language);
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiRadioComponent:" + this.fieldId + " " + errorMessage);
        }
      );
  }

}
