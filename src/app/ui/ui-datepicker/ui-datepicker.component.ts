import {AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';

import {UiBaseComponent} from '../ui-base.component';
import {DateService} from './date.service';
import {LanguageService} from '../../core/language.service';
import {DOCUMENT} from '@angular/platform-browser';

@Component({
  selector: 'qnect-ui-datepicker',
  templateUrl: './ui-datepicker.component.html',
  styleUrls: ['../ui-base.component.scss', './ui-datepicker.component.scss']
})
export class UiDatepickerComponent extends UiBaseComponent implements OnInit, AfterViewInit {

  opened: boolean = false;
  triggerError: boolean = true;
  dt: Date;
  maxDate: Date;
  _minDate: Date;


  constructor(private dateService: DateService, private _languageService: LanguageService, private _elementRef: ElementRef, @Inject(DOCUMENT) private document: any) {
    super(_languageService);
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedOustide = !this._elementRef.nativeElement.contains(targetElement) && document.body.contains(targetElement);

    if (clickedOustide && this.opened) {
      this.toggle();
    }
  }

  ngOnInit() {
    super.ngOnInit();
    if (!this.uiModel.dateFormat) {
      this.uiModel.dateFormat = DateService.getDefaultDateFormat();
    }
    if (this.uiModel.maxDate) {
      this.maxDate = this.dateService.stringToDate(this.uiModel.maxDate, this.uiModel.dateFormat);
    }
    if (this.uiModel.minDate) {
      this._minDate = this.dateService.stringToDate(this.uiModel.minDate, this.uiModel.dateFormat);
    }
    if (this.formControl.value) {
      const val = this.formControl.value;
      if (val instanceof Date) {
        this.setDateValue(val);
      } else {
        this.setDateValueFromInput();
      }
    }
  }

  ngAfterViewInit(): void {
    this.formControl.valueChanges.subscribe(data => {
      this.setDateValueFromInput();
    });
  }

  dateSelected(date: Date) {
    this.toggle();
    this.dt = date;
    this.formControl.setValue(new DatePipe('en-US').transform(this.dt, this.uiModel.dateFormat).toUpperCase());
  }

  showDatePicker() {
    this.toggle();
  }

  toggle() {
    this.opened = !this.opened;
    if (this.opened) {
      this.triggerError = false; // when date picker is shown, do not show error
    } else {
      this.triggerError = true; // when a date is selected, enable checking of errors
    }
  }

  setDateValueFromInput() {
    this.dt = null;
    if (this.formControl.value && this.formControl.value.length > 0) {
      const date: any = this.dateService.stringToDate(this.formControl.value, this.uiModel.dateFormat);
      if (!isNaN(Date.parse(date))) {
        this.setDateValue(date);
        if (this.dt < this._minDate) {
          this.error('min');
        } else {
          this.removeError('min');
        }

        if (this.dt > this.maxDate) {
          this.error('max');
        } else {
          this.removeError('max');
        }
      } else {
        this.triggerError = true;
        this.error('pattern');
        this.opened = false;
      }
    }
  }

  getDateValue() {
    return this.dt;
  }

  setDateValue(date: Date) {
    this.dt = date;
    this.formControl.setValue(new DatePipe('en-US').transform(this.dt, this.uiModel.dateFormat).toUpperCase(), {onlySelf: true, emitEvent: false});
  }

  setValue(value: string) {
    if (value) {
      this.formControl.setValue(value, {onlySelf: true, emitEvent: false});
    }
    this.setDateValueFromInput();
  }

  getValue() {
    console.log(this.uiModel.name + ' : Get date value : ' + this.dateService.dateToString(this.dt, this.uiModel.dateFormat));
    return this.dateService.dateToString(this.dt, this.uiModel.dateFormat);
  }
}
