import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';

@Injectable()
export class DateService {

  private static defaultDateFormat = 'dd/MM/yyyy';
  private static DELIMITERS: string[] = ['/', '-'];

  static convertStringToDate(_date: string, _format: string): Date {
    if (_date) {
      if (_date.toLowerCase() === 'today') {
        return new Date();
      }

      const delimiter = this.getDelimiter(_date);
      const formatLowerCase: string = _format.toLowerCase();
      let formatItems: string[] = formatLowerCase.split(delimiter);

      // Check default format
      if (formatItems.length === 1) {
        formatItems = this.defaultDateFormat.toLowerCase().split(this.getDelimiter(this.defaultDateFormat));
      }

      const dateItems: string[] = _date.split(delimiter);
      const monthIndex: number = this.getIndexFromArray(formatItems, 'm');
      const dayIndex: number = this.getIndexFromArray(formatItems, 'd');
      const yearIndex: number = this.getIndexFromArray(formatItems, 'y');

      if (dayIndex > -1 && yearIndex > -1 && monthIndex > -1 &&
        dateItems[yearIndex] && dateItems[yearIndex].length === 4 && !isNaN(parseInt(dateItems[yearIndex], 0))) {
        if (isNaN(parseInt(dateItems[monthIndex], 0))) {
          return new Date(dateItems[yearIndex] + ' ' + dateItems[monthIndex] + ' ' + dateItems[dayIndex]);
        } else if (parseInt(dateItems[monthIndex], 0) > 0) {
          const month = parseInt(dateItems[monthIndex], 0) - 1;
          return new Date(parseInt(dateItems[yearIndex], 0), month, parseInt(dateItems[dayIndex], 0));
        }
      }
    }
    return null;
  }


  static getIndexFromArray(array: string[], char: string): number {
    let index = -1;
    array.forEach((value: string, key: number) => {
      if (value[0] === char) {
        index = key;
      }
    });
    return index;
  }

  static getDelimiter(_date: string) {
    return this.DELIMITERS.find(each => _date.indexOf(each) > -1);
  }

  static getDefaultDateFormat() {
    return this.defaultDateFormat;
  }

  constructor() {
  }

  stringToDate(_date: string, _format: string): Date {
    return DateService.convertStringToDate(_date, _format);
  }

  dateToString(date: Date, format: string) {
    if (date) {
      let dateFormat: string;
      if (format) {
        dateFormat = format;
      } else {
        dateFormat = DateService.defaultDateFormat;
      }
      return new DatePipe('en-US').transform(date, dateFormat).toUpperCase();
    }
  }

}
