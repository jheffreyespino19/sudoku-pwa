
import { PopoverModule } from '../utilities/components/popover/popover.module';
import { UiOptionsModel } from './ui-options.model';

export class UiModel {

  public type: string;
  public name: string;
  public required: string;
  public disabled: boolean;
  public hidden: boolean;
  public labelText: string;
  public labelSize: string;
  public size: string;
  public placeholder: string;
  public textOnly: boolean;
  public validationRegex: string;
  public defaultValue: string;
  public popover: PopoverModule;
  public options: UiOptionsModel[];
  public staticOptionName: string;
  public disabledOptions: String[];
  public dictionary: any;
  public remoteSource: string;
  public rangeValidator: any;
  public dateFormat: string;
  public minDate: string;
  public maxDate: string;
  public maxlength: number;
  public atLeastOneRequired: string[];
  public hideLabel: boolean;
  public validationRegexList: any;
  public currencyFieldId: string;
  public currencyFieldName: string;
  public currencyPlaceholder: string;
  public currencyOptions: UiOptionsModel[];
  public style: string;
  public hideMandatoryMark: boolean;
  public twelveHourClock: boolean;

}
