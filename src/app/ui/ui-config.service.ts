import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {Observable} from "rxjs";
import {Logger} from "../utilities/service/logger/logger";
import {HttpService} from "../core/http.service";
import {UiModel} from "./ui.model";
import {UICountryService} from "./ui-country.service";
import {UICurrencyService} from "./ui-currency.service";

@Injectable()
export class UiConfigService {

  private config;
  private configUrl: string;

  constructor(private http: HttpService, private _logger: Logger) {
  }

  getUiConfig(): any {
    return this.config;
  }

  setUiConfig(value : any): any {
    this.config = value;
  }

  loadUiConfig(): Observable<any> {
    if (this.config) {
      return Observable.of(this.config);
    } else {
      return this.getConfigJson(this.configUrl).map(response => {
        this.config = response.json();
        this.mapStaticListReference(this.config);
        return this.config;
      });
    }
  }

  getConfigUrl(): string {
    return this.configUrl;
  }

  setConfigUrl(value: string) {
    this.configUrl = value;
  }

  getConfigJson(jsonFile: string): Observable<any> {
    return this.http.get(jsonFile)
      .catch((error: Response) => {
        this._logger.error(error);
        return Observable.throw(error.json());
      });
  }

  getDefaultProduct(jsonFile: string): Observable<string> {
    return this.http.get(jsonFile)
      .map(res => {
        return res.json()['defaultProduct'];
      })
  }

  mapStaticListReference(configObject) {

    for (let entry in configObject) {

      if (configObject.hasOwnProperty(entry)) {

        if (configObject[entry].staticOptionName) {

          if ("COUNTRY_LIST" == configObject[entry].staticOptionName) {
            let countryService: UICountryService = new UICountryService();
            configObject[entry].options = countryService.getCountryOptions();

            //console.log("Setting in static list" + configObject[entry].staticOptionName + " field " + configObject[entry].name);
          }
          else if ("CURRENCY_LIST" == configObject[entry].staticOptionName) {
            let currencyService: UICurrencyService = new UICurrencyService();
            configObject[entry].options = currencyService.getCurrencyOptions();

            //console.log("Setting in static list" + configObject[entry].staticOptionName + " field " + configObject[entry].name);
          }
        }

        if (configObject[entry].currencyStaticOptionName) {
          if ("CURRENCY_LIST" == configObject[entry].currencyStaticOptionName) {
            let currencyService: UICurrencyService = new UICurrencyService();
            configObject[entry].currencyOptions = currencyService.getCurrencyOptions();
          }
        }

      }
    }


  }
}
