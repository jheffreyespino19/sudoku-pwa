import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";
import {UiModel} from "../ui.model";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'qnect-ui-caption',
  templateUrl: './ui-caption.component.html'
})
export class UiCaptionComponent extends UiBaseComponent implements OnInit {

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {

    this.uiModel.labelText = this.fieldConfig['labelText'];
    this.uiModel.dictionary = this.fieldConfig['dictionary'];

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.uiModel.labelText = this.languageService.translateString("labelText", language, this.uiModel.dictionary, this.uiModel.labelText);
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiCaptionComponent:" + this.fieldId + " " + errorMessage);
        }
      );
  }

}
