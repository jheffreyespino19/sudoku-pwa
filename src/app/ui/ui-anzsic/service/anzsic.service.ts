import {Injectable} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/observable/of';

import {AnzsicModel} from './../shared/anzsic-model';
import {Division} from './../shared/division.model';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../../../core/http.service';


@Injectable()
export class AnzsicService {

  private anzsics: AnzsicModel[];
  private industries: Division[];

  constructor(private _http: HttpService) {
  }

  getAllIndustries(country: string, _remoteUrl: string): Observable<Division[]> {
    return this._http.get(_remoteUrl)
      .map(response => {
        this.industries = <Division[]>response.json();
        return this.industries;
      });
  }

  getAllAnzsic(country: string, _remoteUrl: string) {
    return this._http.get(_remoteUrl + country)
      .map(res => <AnzsicModel[]>(res.json()))
      .subscribe(data => {
        for (const anzsic of data) {
          anzsic.display = anzsic.code + ': ' + anzsic.description;
        }
        this.anzsics = data;
      });
  }

  buildAnzsicForm(_anzsicDetails: any): FormGroup {
    return new FormGroup({
      code: new FormControl(_anzsicDetails.code),
      description: new FormControl(_anzsicDetails.description),
      display: new FormControl(_anzsicDetails.display)
    });
  }

  getAnzsic(keyword: string): Observable<AnzsicModel[]> {
    if (this.anzsics) {
      return Observable.of(this.anzsics.filter(x => x.display.toLowerCase().includes(keyword.toLowerCase())));
    } else {
      return Observable.of([]);
    }
  }

  getAnzsicById(id: string): Observable<AnzsicModel[]> {
    if (this.anzsics) {
      return Observable.of(this.anzsics.filter(x => x.code.toLowerCase().startsWith(id.toLowerCase())));
    } else {
      return Observable.of([]);
    }
  }

  getOccupationByAnzsic(id: string): AnzsicModel {
    if (this.anzsics) {
      return this.anzsics.find(x => x.code === id);
    } else {
      return new AnzsicModel;
    }
  }

  getIndustryById(id: string): Division {
    if (this.industries) {
      return this.industries.find(x => x.id === id);
    } else {
      return new Division('', '', []);
    }
  }

  getOccupationById(id: string): Division {
    let occupation = new Division('', '', []);
    if (this.industries) {
      for (const industry of this.industries) {
        for (const occupations of industry.subdivisions) {
          occupation = occupations.subdivisions.find(x => x.id === id);
        }
      }
    }
    return occupation;
  }
}
