export class AnzsicModel {
    public code: string;
    public description: string;
    public display: string;
    public occupationCode: string;
}
