//Model for all anzsic industries in ASIA.
export class Division {
    constructor(
        public id: string,
        public description: string,
        public subdivisions: any[]
    ) {}  
}