import { Component, OnInit, Output, Input, EventEmitter, Inject, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AnzsicService } from './../service/anzsic.service';

import { UiModel } from '../../ui.model'
import { environment } from '../../../../environments/environment';

import { AnzsicModel } from './../shared/anzsic-model';
import { Division } from './../shared/division.model';

@Component({
  selector: 'qnect-anzsic-select',
  templateUrl: './anzsic-select.component.html',
  styleUrls: ['./anzsic-select.component.scss']
})
export class AnzsicSelectComponent implements OnInit {

  @Output("saveForm")
  _saveForm: EventEmitter<any> = new EventEmitter<any>();

  @Input() uiModel: UiModel;

  baseUrl: string;

  crumbs: any[] = [];

  divisions: any[];
  anzsics: AnzsicModel[];

  constructor( @Inject('AnzsicService') private _anzsicService: AnzsicService) { }

  ngOnInit() {
    if (this.uiModel)
      this.baseUrl = this.uiModel.remoteSource;
    this.getAllIndustries();
  }

  getAllIndustries() {
    let _url = (this.baseUrl) ? this.baseUrl : environment.anzsic.findAllIndustries;
    this._anzsicService.getAllIndustries("HKG", _url).subscribe(
      data => {
        this.divisions = Object.assign([], data);
        this.adjustCrumbs(this.divisions);
      });
  }

  selectDivision(div) {
    if (div.divisions) {
      this.anzsics = null;
      this.divisions = Object.assign([], div.divisions);
      this.adjustCrumbs(div);
    } else if (div.subDivisions && div.subDivisions.length > 0) {
      this.anzsics = null;
      this.divisions = Object.assign([], div.subDivisions);
      this.adjustCrumbs(div);
    } else if (div.id && div.id.length > 2) {
      this.getAnzsics(div);
      this.adjustCrumbs(div);
    } else if (Object.keys(div).length > 0){
      this.anzsics = null;
      this.divisions = Object.assign([], div);
      this.adjustCrumbs(div);
    }
  }

  adjustCrumbs(div) {
    if (!div.id) {
      div.description = "All Industries";
    }

    let indexOfDiv = this.crumbs.findIndex(x => x.id == div.id);
    if (indexOfDiv != -1) {
      this.crumbs = Object.assign([], this.crumbs.slice(0, indexOfDiv));
    }

    if (this.crumbs.length < 5 && Object.keys(div.description).length > 0) {
      this.crumbs.push(div);
    }
  }

  getAnzsics(subdivision) {
    this._anzsicService.getAnzsicById(subdivision.id)
      .subscribe(data => {
        this.anzsics = data;
      });
    this.crumbs.push(subdivision.description);
  }

  selectAnzsic(anzsic) {
    this._saveForm.emit(anzsic);
  }

  close() {
    this._saveForm.emit(null);
  }

}
