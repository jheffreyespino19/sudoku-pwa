import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

import { UiBaseComponent } from '../ui-base.component';
import { ModalDialogComponent } from '../../utilities/components/modal-dialog/modal-dialog.component';
import { AutoCompleteDirective } from '../../utilities/components/auto-complete/auto-complete.directive';
import { Observable } from 'rxjs/Observable';


import { AnzsicModel } from './shared/anzsic-model';
import { AnzsicService } from './service/anzsic.service';
import { environment } from '../../../environments/environment';
import {LanguageService} from "../../core/language.service";

@Component({
  selector: 'qnect-ui-anzsic',
  templateUrl: './ui-anzsic.component.html',
  styleUrls: ['./ui-anzsic.component.scss']
})
export class UiAnzsicComponent extends UiBaseComponent implements OnInit {

  anzsicModalHeader: string;

  @ViewChild(ModalDialogComponent)
  public modal: ModalDialogComponent;

  @ViewChild(AutoCompleteDirective)
  _autoCompleteDir: AutoCompleteDirective;

  constructor(private _sanitizer: DomSanitizer, @Inject('AnzsicService') private _anzsicService: AnzsicService, private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.getAllAnzsic("HKG");
  }

  openAnzsicModal() {
    this.anzsicModalHeader = "Select Industry Classification";
    this.modal.show();
  }

  getAllAnzsic(country: string) {
    let _url = (this.uiModel.remoteSource) ? this.uiModel.remoteSource : environment.anzsic.findAll;
    this._anzsicService.getAllAnzsic(country, _url);
  }

  saveAnzsic(selectedAnzsic) {
    this.anzsicSelect(selectedAnzsic);
    this.modal.hide();
  }

  autocompleListFormatter = (data: any): SafeHtml => {
    let html = `<span style="float: left">${data.description}</span><span>${data.code}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  observableSource = (keyword: any) => {
    return this._anzsicService.getAnzsic(keyword);
  }

  anzsicSelect(anzsic) {
    if (anzsic && anzsic !== '') {
      this.formControl.setValue(anzsic.display);
    }
  }

}
