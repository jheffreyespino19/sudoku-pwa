import {Component, OnDestroy, OnInit} from '@angular/core';

import {UiBaseComponent} from '../ui-base.component';
import {UiOptionsModel} from '../ui-options.model';
import {UtilitiesService} from '../../utilities/utilities.service';
import {LanguageService} from "../../core/language.service";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'qnect-ui-dropdown-select',
  templateUrl: './ui-dropdown-select.component.html',
  styleUrls: ['./ui-dropdown-select.component.scss', '../ui-base.component.scss']
})
export class UiDropdownSelectComponent extends UiBaseComponent implements OnInit {

  selectedText: string;
  options: UiOptionsModel[] = [];

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.options = this.uiModel.options;

    // IE Fix, add empty option
    if (UtilitiesService.detectIE() !== false && this.options && this.options.filter(x => x.value === '').length < 1) {
      this.options.unshift({label: '', value: ''});
    }

    this.setSelectedText();
    this.formControl.valueChanges.subscribe(data => {
      this.setSelectedText();
    });

    if (!this.uiModel.placeholder) {
      this.uiModel.placeholder = 'Please Select';
    }

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.uiModel.placeholder = this.languageService.translateString(null, language, this.uiModel.dictionary, this.uiModel.placeholder);
          this.languageService.translateOptions(this.uiModel.options, language);
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiDropdownSelectComponent:" + this.fieldId + " " + errorMessage);
        }
      );

  }

  loadDropDownOptions(options: UiOptionsModel[]) {
    console.log('UiDropdownSelectComponent loadDropDownOptions called ! : ' + options.length);
    this.uiModel.options = options;
  }

  setSelectedText() {
    this.uiModel.options.forEach(each => {
      if (this.formControl.value === each.value) {
        this.selectedText = each.label;
        // break loop
        return false;
      }
    });
  }

  getSelectedTextDropDownSelect() {
    return this.selectedText;
  }
}
