import {Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/observable/of';

import {environment} from '../../../../environments/environment';
import {AddressService} from './address.service';
import {Address} from '../../../claims/model/address.model';
import {HttpService} from '../../../core/http.service';

@Injectable()
export class DefaultAddressService implements AddressService {

  constructor(private _http: HttpService) {
  }

  mapAddressForm(_addressDetails: Address, _addressForm: FormGroup): void {
    _addressForm.get('accumulationRegister').setValue(_addressDetails.accumulationRegister);
    _addressForm.get('unitNo').setValue(_addressDetails.unitNo);
    _addressForm.get('floorNo').setValue(_addressDetails.floorNo);
    _addressForm.get('buildingName').setValue(_addressDetails.buildingName);
    _addressForm.get('buildingStreetNo').setValue(_addressDetails.buildingStreetNo);
    _addressForm.get('streetName').setValue(_addressDetails.streetName);
    _addressForm.get('district').setValue(_addressDetails.district);
  }

  saveAddress(_addressForm: FormGroup): Observable<Address> {
    const floorNo = _addressForm.get('floorNo').value;
    const unitNo = _addressForm.get('unitNo').value;
    const streetNo = _addressForm.get('buildingStreetNo').value;
    const streetName = _addressForm.get('streetName').value;
    const addressDetails: Address = new Address();
    addressDetails.accumulationRegister = _addressForm.get('accumulationRegister').value;
    addressDetails.unitNo = _addressForm.get('unitNo').value;
    addressDetails.floorNo = _addressForm.get('floorNo').value;
    addressDetails.buildingName = _addressForm.get('buildingName').value;
    addressDetails.buildingStreetNo = _addressForm.get('buildingStreetNo').value;
    addressDetails.district = _addressForm.get('district').value;
    addressDetails.postCode = _addressForm.get('postCode').value;
    addressDetails.country = _addressForm.get('country').value;

    addressDetails.addressLine1 = streetNo + (streetNo && streetName ? ' ' : '') + streetName;
    addressDetails.addressLine2 = unitNo + (unitNo && floorNo ? ' ' : '') + floorNo;
    addressDetails.addressLine3 = _addressForm.get('buildingName').value;
    addressDetails.addressLine4 = _addressForm.get('district').value;


    const headers = new Headers({'Content-Type': 'application/json'}); // ... Set content type to JSON
    const options = new RequestOptions({headers: headers}); // Create a request option

    return this._http.put(environment.address.saveurl, addressDetails, options).map((res: Response) => {
      return res.json();
    });
  }

  getAddress(_url: string): Observable<Address[]> {
    if (_url) {
      return this._http.get(_url)
        .map(res => {
          return <any[]>(res.json());
        });
    } else {
      return Observable.of([]);
    }
  }

}
