import { FormGroup } from '@angular/forms'
import { Observable } from 'rxjs/Observable';
import { Address } from '../../../claims/model/address.model';

export interface AddressService{

	mapAddressForm(_addressDetails: Address, _addressForm: FormGroup): void;

	saveAddress(_addressForm: FormGroup) : Observable<Address>;

	getAddress(_url: string) : Observable<Address[]>;

}
