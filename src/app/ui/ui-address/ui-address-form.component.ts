import { Component, OnInit, OnChanges, Output, Input, EventEmitter, Inject, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AddressService } from './service/address.service';
import { Address } from '../../claims/model/address.model';

@Component({
  selector: 'qnect-ui-address-form',
  templateUrl: './ui-address-form.component.html'
})
export class UiAddressFormComponent implements OnInit, OnChanges {

  @Output("saveForm")
  _saveForm: EventEmitter<Address> = new EventEmitter<Address>();

  @Input("addressDetails")
  _addressDetails: Address;

  addressForm: FormGroup;

  constructor( @Inject('AddressService') private _addressService: AddressService) { }

  ngOnInit() {
    this.addressForm = new FormGroup({
      accumulationRegister: new FormControl(),
      unitNo: new FormControl(),
      floorNo: new FormControl(),
      buildingName: new FormControl(),
      buildingStreetNo: new FormControl(),
      streetName: new FormControl(),
      district: new FormControl(),
      postCode: new FormControl()
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this._addressDetails && this.addressForm) {
      this._addressService.mapAddressForm(this._addressDetails, this.addressForm);
    }
  }

  saveAddress() {
    if (this.isFormValid()) {
      if (this._addressDetails) {
        this._addressService.saveAddress(this.addressForm).subscribe(_savedaddress => {
          this._saveForm.emit(_savedaddress);
        });
      }
    }
  }

  isFormValid(): boolean {
    //error message will not be displayed unless forms are not touched
    Object.keys(this.addressForm.controls).forEach(key => {
      this.addressForm.get(key).markAsTouched();
    });
    return this.addressForm.valid;
  }

  isValidated(): boolean {
    try {
      return this.addressForm.get('accumulationRegister').value == 'NONE'
    } catch (ex) {
      return false;
    }
  }

  close() {
    this._saveForm.emit(null);
  }

}
