import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

import { UiBaseComponent } from '../ui-base.component';
import { ModalDialogComponent } from '../../utilities/components/modal-dialog/modal-dialog.component';
import { AutoCompleteDirective } from '../../utilities/components/auto-complete/auto-complete.directive';
import { Observable } from 'rxjs/Observable';

import { AddressService } from './service/address.service';
import { environment } from '../../../environments/environment';
import { Address } from '../../claims/model/address.model';
import {LanguageService} from "../../core/language.service";


@Component({
  selector: 'qnect-ui-address',
  templateUrl: './ui-address.component.html',
  styleUrls: ['./ui-address.component.scss']
})
export class UiAddressComponent extends UiBaseComponent implements OnInit {

  @ViewChild(ModalDialogComponent)
  public modal: ModalDialogComponent;

  @ViewChild(AutoCompleteDirective)
  _autoCompleteDir: AutoCompleteDirective;

  readOnly: boolean = false;

  constructor(private _sanitizer: DomSanitizer, @Inject('AddressService') private _addressService: AddressService, private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  autocompleListFormatter = (data: Address): SafeHtml => {
    let html = `<span>${data.addressLine}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  isEmptyObject(obj) {
    return obj == "" || obj == null;
  }

  clearAddress() {
    this.formControl.setValue(null);
    this.readOnly = false;
  }

  openAddressFormModal() {
    this.modal.show();
  }

  updateAddress(savedAddress: Address) {
    if (savedAddress) {
      Object.assign(this.formControl.value, savedAddress);
      this._autoCompleteDir.selectNewValue(this.formControl.value);
    } else if (this.formControl.value.accumulationRegister == "NONE") {
      this.clearAddress();
    }
    this.modal.hide();
  }

  observableSource = (keyword: any): Observable<any[]> => {
    let _url = (this.uiModel.remoteSource) ? this.uiModel.remoteSource : environment.address.findUrl;
    return this._addressService.getAddress((keyword) ? _url + "?addressLine=" + keyword : "");
  }

  addressSelect(address: Address) {
    if (address && address.accumulationRegister != '') {
      if (address.accumulationRegister == "NONE") {
        this.openAddressFormModal();
      }
      this.readOnly = true;
    }

  }

}
