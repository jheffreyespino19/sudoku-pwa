import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';

import {UiBaseComponent} from '../ui-base.component';
import {LanguageService} from '../../core/language.service';
import {AbstractControl, Validators} from '@angular/forms';
import {UiCustomValidatorFactory} from '../validators/ui-custom-validator.factory';
import {UiValidators} from '../validators/ui-validator';

@Component({
  selector: 'qnect-ui-moneyfield',
  templateUrl: './ui-moneyfield.component.html',
  styleUrls: ['../ui-base.component.scss']
})
export class UiMoneyfieldComponent extends UiBaseComponent implements OnInit {

  @ViewChild('inputField') inputField: ElementRef;

  currencyFormControl: AbstractControl;
  currencySelectedText: string = '';
  amountFieldSize: string = '7';

  constructor(private renderer: Renderer2, private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.formControl.valueChanges.subscribe((value) => {
      if (value === '' || value === '0') {
        this.formControl.setValue(null);
      }
      this.currencyFormControl.updateValueAndValidity();
    });

    if (this.uiModel.currencyFieldName) {
      this.currencyFormControl = this.formGroup.get(this.uiModel.currencyFieldName);

      const validatorList = [];
      // if (this.uiModel.required) validatorList.push(Validators.required);
      if (this.uiModel.currencyFieldId) validatorList.push(UiValidators.moneyCurrencyValidation(this.formControl));
      this.currencyFormControl.setValidators(validatorList);

      this.setSelectedText();
      this.currencyFormControl.valueChanges.subscribe(data => {
        this.setSelectedText();
      });
    } else {
      this.amountFieldSize = '12';
    }

  }

  setValue(value: string) {
    this.formControl.setValue(value);
    if (this.inputField) {
      // blur the field to trigger formatting
      this.inputField.nativeElement.dispatchEvent(new Event('blur'));
    }
  }

  onKeyPress(event: any) {
    const pattern = /^[0-9]*$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  setSelectedText() {
    this.uiModel.currencyOptions.forEach(each => {
      if (this.currencyFormControl.value === each.value) {
        this.currencySelectedText = each.label;
        // break loop
        return false;
      }
    });
  }

}
