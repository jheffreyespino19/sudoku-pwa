import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";

@Component({
  selector: 'qnect-ui-textfield',
  templateUrl: './ui-textfield.component.html',
  styleUrls: ['../ui-base.component.scss']
})
export class UiTextfieldComponent extends UiBaseComponent implements OnInit {

  private dataList: string;

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.uiModel.options) {
      this.dataList = this.fieldId + 'DataList';
    }
  }

}
