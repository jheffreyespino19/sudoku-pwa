import {Component, OnInit, OnChanges, Input, OnDestroy} from '@angular/core';
import { UiValidators } from '../../validators/ui-validator';
import {LanguageService} from "../../../core/language.service";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'ui-error',
  templateUrl: './ui-error.component.html',
  styleUrls: ['./ui-error.component.scss']
})
export class UiErrorComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  inputErrors: any;
  @Input("dictionary")
  _dictionary = {}
  errorMessages: any[];

  protected language : string;
  protected ngUnsubscribe: Subject<void> = new Subject<void>();


  constructor(private languageService: LanguageService) { }

  ngOnInit() {

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.language = language;
          this.translateErrorMessages(this.language);
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiErrorComponent:" + errorMessage);
        }
      );

  }

  translateErrorMessages(language : string) {

    if (this.errorMessages) {
      for (let error of this.errorMessages) {

        // For original text we need the english error.
        if (this._dictionary) {
          let dictionaryEntry = this._dictionary[error.code];
          if (dictionaryEntry) {
            error.message = dictionaryEntry["en"];
          }
        }

        error.message = this.languageService.translateString(error.code, language, this._dictionary, error.message);
      }
    }
  }


  ngOnChanges(changes: any): void {
    var errors: any = changes.inputErrors.currentValue;
    this.errorMessages = UiValidators.getValidatorErrors(errors);
    this.translateErrorMessages(this.language);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
