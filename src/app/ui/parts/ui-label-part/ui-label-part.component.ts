import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { UiModel } from '../../ui.model'
import {Subject} from "rxjs/Subject";
import {LanguageService} from "../../../core/language.service";

@Component({
  selector: 'qnect-ui-label-part',
  templateUrl: './ui-label-part.component.html',
  styleUrls: ['./ui-label-part.component.scss', '../../ui-base.component.scss']
})
export class UiLabelPartComponent implements OnInit, OnDestroy {

  @Input() fieldId: string;
  @Input() uiModel: UiModel;

  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private languageService: LanguageService) { }

  ngOnInit() {

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          this.uiModel.labelText = this.languageService.translateString("labelText", language, this.uiModel.dictionary, this.uiModel.labelText);
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiLabelPartComponent:" + this.fieldId + " " + errorMessage);
        }
      );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
