import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {UtilitiesModule} from '../utilities/utilities.module';

import {UiComponent} from './ui.component';
import {UiTextfieldComponent} from './ui-textfield/ui-textfield.component';
import {UiMoneyfieldComponent} from './ui-moneyfield/ui-moneyfield.component';
import {UiUnknownTypeComponent} from './ui-unknown-type/ui-unknown-type.component';
import {UiButtonGrpSingleSelectComponent} from './ui-button-grp-single-select/ui-button-grp-single-select.component';
import {UiMultipleSelectComponent} from './ui-multiple-select/ui-multiple-select.component';
import {UiDropdownSelectComponent} from './ui-dropdown-select/ui-dropdown-select.component';
import {UiRadioComponent} from './ui-radio/ui-radio.component';
import {UiLabelPartComponent} from './parts/ui-label-part/ui-label-part.component';
import {UiCaptionComponent} from './ui-caption/ui-caption.component';
import {UiLanguageControlComponent} from './ui-language-control/ui-language-control.component';
import {UiDatepickerComponent} from './ui-datepicker/ui-datepicker.component';
import {DateService} from './ui-datepicker/date.service';
import {UiFormService} from './ui-form.service';
import {UiTimepickerComponent} from './ui-timepicker/ui-timepicker.component';
import {UiOptionsService} from './ui-options.service';
import {UiOptionsPipe} from './ui-options.pipe';
import {UiErrorComponent} from './parts/ui-error/ui-error.component';
import {UiAddressComponent} from './ui-address/ui-address.component';
import {UiAddressFormComponent} from './ui-address/ui-address-form.component';
import {DefaultAddressService} from './ui-address/service/default-address.service';
import {AddressService} from './ui-address/service/address.service';
import {AnzsicService} from './ui-anzsic/service/anzsic.service';
import {UiAnzsicComponent} from './ui-anzsic/ui-anzsic.component';
import {AnzsicSelectComponent} from './ui-anzsic/anzsic-select/anzsic-select.component';
import {UiTextareaComponent} from './ui-textarea/ui-textarea.component';
import {ToggleDisplayOnParentValueEnterDirective} from './directives/toggle-display-on-parent-value-enter.directive';
import {ToggleDisplayOnParentValueChangeDirective} from './directives/toggle-display-on-parent-value-change.directive';
import {ParentPercentDefaulterDirective} from './directives/parent-percent-defaulter.directive';
import {ParentTotalAggregatorDirective} from './directives/parent-total-aggregator';
import {MaxDefaultValueValidatorDirective} from './directives/max-default-value-validator.directive';
import {ToggleDisableOnParentValueChangeDirective} from './directives/toggle-disable-on-parent-value-change.directive';
import {ClearValueOnHideDirective} from './directives/clear-value-on-hide.directive';
import {ClearValueOnParentValueClearDirective} from './directives/clear-value-on-parent-value-clear.directive';
import {ChangeValueOnParentValueChangeDirective} from './directives/change-value-on-parent-value-change.directive';
import {DateValidatorDirective} from './validators/directives/date-validator.directive';
import {ToggleEnableOnParentValueChangeDirective} from './directives/toggle-enable-on-parent-value-change.directive';


@NgModule({
  declarations: [
    UiComponent,
    UiTextfieldComponent,
    UiMoneyfieldComponent,
    UiUnknownTypeComponent,
    UiButtonGrpSingleSelectComponent,
    UiMultipleSelectComponent,
    UiDropdownSelectComponent,
    UiRadioComponent,
    UiLabelPartComponent,
    UiCaptionComponent,
    UiLanguageControlComponent,
    UiDatepickerComponent,
    UiErrorComponent,
    UiAddressComponent,
    UiAddressFormComponent,
    UiAnzsicComponent,
    UiTimepickerComponent,
    AnzsicSelectComponent,
    UiTextareaComponent,
    ToggleDisplayOnParentValueEnterDirective,
    ToggleDisplayOnParentValueChangeDirective,
    ParentPercentDefaulterDirective,
    ParentTotalAggregatorDirective,
    MaxDefaultValueValidatorDirective,
    ToggleDisableOnParentValueChangeDirective,
    ToggleEnableOnParentValueChangeDirective,
    ClearValueOnHideDirective,
    ClearValueOnParentValueClearDirective,
    ChangeValueOnParentValueChangeDirective,
    UiOptionsPipe,
    DateValidatorDirective

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    UtilitiesModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,

    UiComponent,
    UiTextfieldComponent,
    UiMoneyfieldComponent,
    UiUnknownTypeComponent,
    UiButtonGrpSingleSelectComponent,
    UiMultipleSelectComponent,
    UiDropdownSelectComponent,
    UiRadioComponent,
    UiLabelPartComponent,
    UiCaptionComponent,
    UiLanguageControlComponent,
    UiDatepickerComponent,
    UiErrorComponent,
    UiAddressComponent,
    UiAddressFormComponent,
    UiAnzsicComponent,
    AnzsicSelectComponent,
    UiTextareaComponent,
    ToggleDisplayOnParentValueEnterDirective,
    ToggleDisplayOnParentValueChangeDirective,
    ParentPercentDefaulterDirective,
    ParentTotalAggregatorDirective,
    MaxDefaultValueValidatorDirective,
    ToggleDisableOnParentValueChangeDirective,
    ToggleEnableOnParentValueChangeDirective,
    ClearValueOnHideDirective,
    ClearValueOnParentValueClearDirective,
    ChangeValueOnParentValueChangeDirective,
    UiOptionsPipe,
    DateValidatorDirective

  ],
  providers: [
    AnzsicService,
    DefaultAddressService,
    UiOptionsService,
    UiFormService,
    {
      provide: 'AddressService', useClass: DefaultAddressService
    },
    {
      provide: 'AnzsicService', useClass: AnzsicService
    },
    DateService
  ]
})
export class UiModule {
}
