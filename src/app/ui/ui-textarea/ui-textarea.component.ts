import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";

@Component({
  selector: 'qnect-ui-textarea',
  templateUrl: './ui-textarea.component.html',
  styleUrls: ['../ui-base.component.scss']
})
export class UiTextareaComponent extends UiBaseComponent implements OnInit {

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

}
