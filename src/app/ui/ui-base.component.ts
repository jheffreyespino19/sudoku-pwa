import {EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {AbstractControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {UiModel} from './ui.model';
import {UiCustomValidatorFactory} from './validators/ui-custom-validator.factory';
import {UiValidators} from './validators/ui-validator';
import {UiFormService} from './ui-form.service';
import {UiOptionsModel} from './ui-options.model';
import {Subject} from "rxjs/Subject";
import {LanguageService} from "../core/language.service";

export abstract class UiBaseComponent implements OnDestroy{

  uiModel: UiModel = new UiModel();

  @Input() fieldId: string;
  @Input() fieldConfig = {};
  @Input() formGroup: FormGroup;
  @Input() value: String;
  @Input() dynamicOptions: UiOptionsModel[];

  @Output() uiInit = new EventEmitter<UiModel>();

  private _formControl: AbstractControl;
  /* put common error codes here */
  errorCodes = UiValidators.ERROR_CODES;

  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(protected languageService: LanguageService) {
  }

  ngOnInit() {
    this.mapUiModel();

    //console.log("this.uiModel.name : " + this.uiModel.name);
    this.formControl = this.formGroup.get(this.uiModel.name);
    this.formControl.setValidators(this.setupValidatorList(null));
    this.setupControlProperties();
    // this.uiInit.emit(this.uiModel);

    // Translate if required
    this.languageService.getLanguage()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        language => {
          if (this.uiModel.placeholder) {
            this.uiModel.placeholder = this.languageService.translateString(null, language, this.uiModel.dictionary, this.uiModel.placeholder);
          }
        },
        error => {
          let errorMessage = "Error with language";
          console.error("UiDropdownSelectComponent:" + this.fieldId + " " + errorMessage);
        }
      );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  setupCustomValidatorList(customValidators) {
    this.formControl = this.formGroup.get(this.uiModel.name);
    this.formControl.setValidators(this.setupValidatorList(customValidators));
  }

  setupValidatorList(customValidators) {
    let validatorList = [];
    if (this.uiModel.required) validatorList.push(Validators.required);
    if (this.uiModel.maxlength) validatorList.push(Validators.maxLength(this.uiModel.maxlength));
    if (this.uiModel.validationRegex) validatorList.push(Validators.pattern(this.uiModel.validationRegex));
    UiCustomValidatorFactory.getCustomValidators(this.uiModel).forEach(each => {
      validatorList.push(each);
    });

    if (customValidators) {
      customValidators.forEach(each => {
        validatorList.push(each);
      });
    }

    return validatorList;
  }

  setupControlProperties() {
    if (this.uiModel.disabled) this.formControl.disable();
    if (!this.formControl.value && this.uiModel.defaultValue) {
      this.formControl.setValue(this.uiModel.defaultValue);
    }
  }

  mapUiModel() {
    this.uiModel.type = this.fieldConfig['type'];
    this.uiModel.name = this.fieldConfig['name'];
    this.uiModel.required = this.fieldConfig['required'];
    this.uiModel.disabled = this.fieldConfig['disabled'];
    this.uiModel.hidden = this.fieldConfig['hidden'];
    this.uiModel.labelText = this.fieldConfig['labelText'];
    this.uiModel.labelSize = this.fieldConfig['labelSize'];
    this.uiModel.size = this.fieldConfig['size'];
    this.uiModel.placeholder = this.fieldConfig['placeholder'];
    this.uiModel.textOnly = this.fieldConfig['textOnly'];
    this.uiModel.validationRegex = this.fieldConfig['validationRegex'];
    this.uiModel.defaultValue = this.fieldConfig['defaultValue'];
    this.uiModel.popover = this.fieldConfig['popover'];
    this.uiModel.options = this.fieldConfig['options'];
    this.uiModel.disabledOptions = this.fieldConfig['disabledOptions'];
    this.uiModel.dictionary = this.fieldConfig['dictionary'];
    this.uiModel.remoteSource = this.fieldConfig['remoteSource'];
    this.uiModel.rangeValidator = this.fieldConfig['rangeValidator'];
    this.uiModel.disabled = this.fieldConfig['disabled'];
    this.uiModel.dateFormat = this.fieldConfig['dateFormat'];
    this.uiModel.minDate = this.fieldConfig['minDate'];
    this.uiModel.maxDate = this.fieldConfig['maxDate'];
    this.uiModel.maxlength = this.fieldConfig['maxlength'];
    this.uiModel.atLeastOneRequired = this.fieldConfig['atLeastOneRequired'];
    this.uiModel.hideLabel = this.fieldConfig['hideLabel'];
    this.uiModel.currencyFieldId = this.fieldConfig['currencyFieldId'];
    this.uiModel.currencyFieldName = this.fieldConfig['currencyFieldName'];
    this.uiModel.currencyPlaceholder = this.fieldConfig['currencyPlaceholder'];
    this.uiModel.currencyOptions = this.fieldConfig['currencyOptions'];
    this.uiModel.style = this.fieldConfig['style'];
    this.uiModel.hideMandatoryMark = this.fieldConfig['hideMandatoryMark'];
    this.uiModel.twelveHourClock = this.fieldConfig['twelveHourClock'];

    // Usage: key-value pair
    this.uiModel.validationRegexList = this.fieldConfig['validationRegexList'];

    if (this.dynamicOptions) {
      this.uiModel.options = this.dynamicOptions;
    }
  }


  get formControl(): AbstractControl {
    return this._formControl;
  }

  set formControl(value: AbstractControl) {
    this._formControl = value;
  }

  hide() {
    this.uiModel.hidden = true;
  }

  show() {
    this.uiModel.hidden = false;
  }

  isHidden(): boolean {
    return this.uiModel.hidden;
  }

  enable() {
    this.uiModel.disabled = false;
    this.formControl.enable({emitEvent: false});
  }

  disable() {
    this.uiModel.disabled = true;
    this.formControl.disable({emitEvent: false});
  }

  isDisabled(): boolean {
    return this.uiModel.disabled;
  }

  setDefaultValue(defaultValue: string): void {
    this.uiModel.defaultValue = defaultValue;
  }

  getDefaultValue(): string {
    return this.uiModel.defaultValue;
  }

  setValue(value: string) {
    this.formControl.setValue(value);
  }

  getValue(): string {
    return this.formControl.value;
  }

  valueChanges(): Observable<any> {
    return this.formControl.valueChanges;
  }

  error(errorCode: string) {
    UiFormService.mapFormControlError(this.formControl,  errorCode ? [errorCode] : null);
  }

  getOptionLabel(value: string): string {
    if (this.uiModel.options) {
      const option: UiOptionsModel = this.uiModel.options.find(each => each.value === value);
      return option.label;
    }
    return '';
  }

  update(): void {
    this.formControl.updateValueAndValidity({onlySelf: true, emitEvent: false});
  }

  set textOnly(textOnly: boolean) {
    this.uiModel.textOnly = textOnly;
  }

  set required(required: boolean) {
    this.uiModel.required = required ? 'true' : null;
    this.formControl.setValidators(this.setupValidatorList(null));
  }

  set hideLabel(hideLabel: boolean) {
    this.uiModel.hideLabel = hideLabel;
  }

  set maxValue(maxValue: number) {
    if (!this.uiModel.rangeValidator) {
      this.uiModel.rangeValidator = {};
    }
    this.uiModel.rangeValidator.max = maxValue;
  }

  get maxValue(): number {
    if (!this.uiModel.rangeValidator) {
      this.uiModel.rangeValidator = {};
    }
    return this.uiModel.rangeValidator.max;
  }

  set minValue(minValue: number) {
    if (!this.uiModel.rangeValidator) {
      this.uiModel.rangeValidator = {};
    }
    this.uiModel.rangeValidator.min = minValue;
  }

  get minValue(): number {
    if (!this.uiModel.rangeValidator) {
      this.uiModel.rangeValidator = {};
    }
    return this.uiModel.rangeValidator.min;
  }

  set validationRegex(regex: string) {
    this.uiModel.validationRegex = regex;
    this.formControl.setValidators(this.setupValidatorList(null));
  }

  removeError(error: string) {
    UiFormService.removeError(this.formControl, error);
  }
}
