import { Component, OnInit } from '@angular/core';

import { UiBaseComponent } from '../ui-base.component';
import {LanguageService} from "../../core/language.service";

@Component({
  selector: 'qnect-ui-button-grp-single-select',
  templateUrl: './ui-button-grp-single-select.component.html',
  styleUrls: ['../ui-base.component.scss']
})
export class UiButtonGrpSingleSelectComponent extends UiBaseComponent implements OnInit {

  constructor(private _languageService: LanguageService) {
    super(_languageService);
  }

  ngOnInit() {
  }

}
