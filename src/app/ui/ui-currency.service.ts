import {UiOptionsModel} from "./ui-options.model";
export class UICurrencyService {
  getCurrencyOptions(): UiOptionsModel[] {
    return [
      {"value":"SGD" ,"label":"SGD"},
      {"value":"USD" ,"label":"USD"},
      {"value":"AUD" ,"label":"AUD"},
      {"value":"IDR" ,"label":"IDR"},
      {"value":"MYR" ,"label":"MYR"},
      {"value":"GBP" ,"label":"GBP"},
      {"value":"EUR" ,"label":"EUR"},
      {"value":"AED" ,"label":"AED"},
      {"value":"JPY" ,"label":"JPY"},
      {"value":"CAD" ,"label":"CAD"},
      {"value":"CNY" ,"label":"CNY"},
      {"value":"HKD" ,"label":"HKD"},
      {"value":"" ,"label":"------"},
      {"value":"AED" ,"label":"AED"},
      {"value":"BND" ,"label":"BND"},
      {"value":"CHF" ,"label":"CHF"},
      {"value":"DKK" ,"label":"DKK"},
      {"value":"INR" ,"label":"INR"},
      {"value":"KRW" ,"label":"KRW"},
      {"value":"NOK" ,"label":"NOK"},
      {"value":"NZD" ,"label":"NZD"},
      {"value":"PHP" ,"label":"PHP"},
      {"value":"SEK" ,"label":"SEK"},
      {"value":"THB" ,"label":"THB"},
      {"value":"TRY" ,"label":"TRY"},
      {"value":"TWD" ,"label":"TWD"},
      {"value":"VND" ,"label":"VND"}
      ]
  }
  }
