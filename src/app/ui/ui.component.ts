import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, ValidatorFn } from '@angular/forms';

import {UiConfigService} from './ui-config.service';
import {UiTextfieldComponent} from './ui-textfield/ui-textfield.component';
import {UiMoneyfieldComponent} from './ui-moneyfield/ui-moneyfield.component';
import {UiUnknownTypeComponent} from './ui-unknown-type/ui-unknown-type.component';
import {UiButtonGrpSingleSelectComponent} from './ui-button-grp-single-select/ui-button-grp-single-select.component';
import {UiMultipleSelectComponent} from "./ui-multiple-select/ui-multiple-select.component";
import {UiDropdownSelectComponent} from "./ui-dropdown-select/ui-dropdown-select.component";
import {UiRadioComponent} from "./ui-radio/ui-radio.component";
import {UiCaptionComponent} from './ui-caption/ui-caption.component';
import {UiLanguageControlComponent} from './ui-language-control/ui-language-control.component';
import {UiOptionsModel} from './ui-options.model';
import {UiAddressComponent} from './ui-address/ui-address.component';
import {UiAnzsicComponent} from './ui-anzsic/ui-anzsic.component';
import {UiDatepickerComponent} from './ui-datepicker/ui-datepicker.component';
import {UiTextareaComponent} from './ui-textarea/ui-textarea.component';
import {UiTimepickerComponent} from './ui-timepicker/ui-timepicker.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'qnect-ui',
  template: '<span #dynamicUiComponent></span>',
  styles: [],
  entryComponents: [
    UiTextfieldComponent,
    UiTextareaComponent,
    UiMoneyfieldComponent,
    UiButtonGrpSingleSelectComponent,
    UiMultipleSelectComponent,
    UiDropdownSelectComponent,
    UiRadioComponent,
    UiCaptionComponent,
    UiLanguageControlComponent,
    UiAddressComponent,
    UiAnzsicComponent,
    UiDatepickerComponent,
    UiTimepickerComponent,
    UiUnknownTypeComponent
  ]
})
export class UiComponent implements OnInit {

  @Input() fieldId: string;
  @Input() value: string;
  @Input() formGroup: FormGroup;
  @Input() option: string;
  @Input() dynamicOptions: UiOptionsModel[];
  fieldConfig: any = {};

  @Output() uiComponentCreated = new EventEmitter<string>();

  constructor(private uiConfigService: UiConfigService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private cdr: ChangeDetectorRef) {
  }

  // Get tag child component will be placed
  @ViewChild('dynamicUiComponent', {read: ViewContainerRef}) target: ViewContainerRef;
  private componentRef: ComponentRef<any>;

  // Child components
  private children = {
    "textfield": UiTextfieldComponent,
    "textarea": UiTextareaComponent,
    "moneyfield": UiMoneyfieldComponent,
    "btnGrpSingleSelect": UiButtonGrpSingleSelectComponent,
    "multiSelect": UiMultipleSelectComponent,
    "dropdownSelect": UiDropdownSelectComponent,
    "radio": UiRadioComponent,
    "caption": UiCaptionComponent,
    "languageControl": UiLanguageControlComponent,
    "address": UiAddressComponent,
    "anzsic": UiAnzsicComponent,
    "datepicker": UiDatepickerComponent,
    "timepicker": UiTimepickerComponent,
    "unknown": UiUnknownTypeComponent,
    undefined: UiUnknownTypeComponent
  };

  // Pass through value to child component
  renderComponent() {
    if (this.componentRef) {
      this.componentRef.instance.fieldId = this.fieldId;
      this.componentRef.instance.fieldConfig = this.fieldConfig;
      this.componentRef.instance.formGroup = this.formGroup;
      this.componentRef.instance.value = this.value;
      this.componentRef.instance.option = this.option;
      this.componentRef.instance.dynamicOptions = this.dynamicOptions;
    }
  }

  // Compile child component
  compileChild() {
    let type: string = "unknown";
    if (this.fieldConfig && this.fieldConfig["type"]) {
      type = this.fieldConfig["type"];
    }
    let childComponent = this.children[type];
    // Resolve child component
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(childComponent);
    this.componentRef = this.target.createComponent(componentFactory);
    this.renderComponent();
  }

  // Pass through value to child component when value changes
  ngOnChanges(changes: Object) {
    this.renderComponent();
  }

  ngOnInit() {

    this.uiConfigService.loadUiConfig().subscribe(
      data => {
        this.fieldConfig = data[this.fieldId];
        this.compileChild();
        try {
          // if ui component is already destroyed before this code block is finished
          this.cdr.detectChanges();
          this.uiComponentCreated.emit(this.fieldId);
        } catch (ex) {

        }
      }
    );
  }

  setupCustomValidatorList(customValidators) {
    this.componentRef.instance.setupCustomValidatorList(customValidators);
  }

  loadDropDownOptions(options: UiOptionsModel[]) {
    if (this.componentRef && typeof this.componentRef.instance.loadDropDownOptions === 'function') {
      this.componentRef.instance.loadDropDownOptions(options);
    }
  }

  hide(): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.hide === 'function') {
      this.componentRef.instance.hide();
    }
    return this;
  }
  show(): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.show === 'function') {
      this.componentRef.instance.show();
    }
    return this;
  }
  isHidden(): boolean {
    if (this.componentRef && typeof this.componentRef.instance.isHidden === 'function') {
      return this.componentRef.instance.isHidden();
    }
  }
  enable(): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.enable === 'function') {
      this.componentRef.instance.enable();
    }
    return this;
  }
  disable(opts?: any): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.disable === 'function') {
      this.componentRef.instance.disable(opts);
    }
    return this;
  }
  isDisabled(): boolean {
    if (this.componentRef && typeof this.componentRef.instance.isDisabled === 'function') {
      return this.componentRef.instance.isDisabled();
    }
  }
  setDefaultValue(defaultValue: string): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.setDefaultValue === 'function') {
      this.componentRef.instance.setDefaultValue(defaultValue);
    }
    return this;
  }
  getDefaultValue(): string {
    if (this.componentRef && typeof this.componentRef.instance.getDefaultValue === 'function') {
      return this.componentRef.instance.getDefaultValue();
    }
    return null;
  }
  setValue(value: string, options?: any): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.setValue === 'function') {
      this.componentRef.instance.setValue(value, options);
    }
    return this;
  }
  getValue(): string {
    if (this.componentRef && typeof this.componentRef.instance.getValue === 'function') {
      return this.componentRef.instance.getValue();
    }
  }
  valueChanges(): Observable<any> {
    if (this.componentRef && typeof this.componentRef.instance.valueChanges === 'function') {
      return this.componentRef.instance.valueChanges();
    }
  }
  error(errorCode: string): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance.error === 'function') {
      this.componentRef.instance.error(errorCode);
    }
    return this;
  }
  getOptionLabel(value: string): string {
    if (this.componentRef && typeof this.componentRef.instance) {
      return this.componentRef.instance.getOptionLabel(value);
    }
  }
  getDateValue() {
    if (this.componentRef && typeof this.componentRef.instance.getDateValue === 'function') {
      return this.componentRef.instance.getDateValue();
    }
  }
  setDateValue(date : Date) {
    if (this.componentRef && typeof this.componentRef.instance.setDateValue === 'function') {
      return this.componentRef.instance.setDateValue(date);
    }
  }

  getTimeValue() {
    if (this.componentRef && typeof this.componentRef.instance.getTimeValue === 'function') {
      return this.componentRef.instance.getTimeValue();
    }
  }

  setTimeValue(date: Date) {
    if (this.componentRef && typeof this.componentRef.instance.setTimeValue === 'function') {
      return this.componentRef.instance.setTimeValue(date);
    }
  }

  getSelectedTextDropDownSelect() {
    if (this.componentRef && typeof this.componentRef.instance.getSelectedTextDropDownSelect === 'function') {
      return this.componentRef.instance.getSelectedTextDropDownSelect();
    }
  }

  checkTimePickerError() {
    if (this.componentRef && typeof this.componentRef.instance.checkTimePickerError === 'function') {
      return this.componentRef.instance.checkTimePickerError();
    }
  }

  update(): UiComponent {
    if (this.componentRef && typeof this.componentRef.instance) {
      this.componentRef.instance.update();
    }
    return this;
  }

  set validationRegex(regex: string){
    if (this.componentRef && typeof this.componentRef.instance) {
      this.componentRef.instance.validationRegex = regex;
    }
  }

  set required(required: boolean) {
    if (this.componentRef && typeof this.componentRef.instance) {
      this.componentRef.instance.required = required;
    }
  }
}
