export interface IJsonMetaData<T> {
  name?: string;
  clazz?: { new(): T };
}
export function JsonProperty<T>(metadata?: IJsonMetaData<T> | string): any {
  if (metadata instanceof String || typeof metadata === 'string') {
    return Reflect.metadata('jsonProperty', {
      name: metadata,
      clazz: undefined
    });
  } else {
    const metadataObj = <IJsonMetaData<T>>metadata;
    return Reflect.metadata('jsonProperty', {
      name: metadataObj ? metadataObj.name : undefined,
      clazz: metadataObj ? metadataObj.clazz : undefined
    });
  }
}




